package com.example.consultainss

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var digito_cartao: Int = 0
    var radioGroup_checked: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Consulta pagamento INSS"


        activity_main.setOnClickListener {
            it.hideKeyboard()
        }



        // Ação de click no botão Resultado
        btResultado.setOnClickListener {
            try{
                val ultimodigito = etUltimoDigito.text.toString().toInt()
                digito_cartao = ultimodigito
                radioGroup_checked = radioGroup.checkedRadioButtonId
                val radioButton_checked: RadioButton = findViewById(radioGroup_checked)

                val intent = Intent(this, ResultadoActivity::class.java)
                intent.putExtra("radiobuttonChecked", radioButton_checked.text)
                intent.putExtra("digitoCartao", digito_cartao)
                startActivity(intent)
            }
            catch (e: Exception){
                error()
                etUltimoDigito.setError("Campo obrigatório")
            }
        }

    }

    // Quando o botão de voltar for pressionado na MainActivity
    // O sistema retorna um AlertDialog para evitar
    // que o usuario saia sem querer do aplicativo.
    override fun onBackPressed() {
        val alertdialog =
            AlertDialog.Builder(this)
        alertdialog.setMessage("Você realmente deseja sair?")

        alertdialog.setPositiveButton(
            "Sim"
        ) { _, _ ->
            this@MainActivity.finish()
        }

        alertdialog.setNegativeButton(
            "Não"
        ) { dialog, _ -> dialog.cancel() }

        alertdialog.create()
        alertdialog.show()
    }

    // Infla Main Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.compartilhar -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                val shareBody = "Consulte o pagamento de seu beneficio INSS: https://play.google.com/store/apps/details?id=com.example.consultainss "
                val shareSubject = "Calendário de pagamentos INSS"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject)

                startActivity(Intent.createChooser(shareIntent, "Compartilhe com seus amigos"))
            }

        }
        return super.onOptionsItemSelected(item)
    }
    // Mensagem Toast de Erro
    private fun error(){
        Toast.makeText(this, "Preencha todos campos", Toast.LENGTH_SHORT).show()
    }

    // Função de esconder o teclado
    fun View.hideKeyboard() {
        val inputMethodManager = context!!.getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
    }
}