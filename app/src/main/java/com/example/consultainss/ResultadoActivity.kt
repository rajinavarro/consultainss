package com.example.consultainss

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.view.Gravity.CENTER
import android.view.Gravity.START
import android.widget.*
import kotlinx.android.synthetic.main.activity_resultado.*


class ResultadoActivity : AppCompatActivity() {
    val mesesDoAno = arrayOf(
        "Janeiro",
        "Fevereiro",
        "Março",
        "Abril",
        "Maio",
        "Junho",
        "Julho",
        "Agosto",
        "Setembro",
        "Outubro",
        "Novembro",
        "Dezembro"
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)
        supportActionBar?.title = "Calendário de pagamentos"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        calcular()
    }
    // Infla o Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }
    // Ações de quando um item do menu é selecionado
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.compartilhar -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                val shareBody: String = "Consulte o pagamento de seu beneficio INSS: https://play.google.com/store/apps/details?id=com.example.consultainss"
                val shareSubject = "Calendário de pagamentos INSS"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject)

                startActivity(Intent.createChooser(shareIntent, "Compartilhe"))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Botão de voltar ao Menu
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    // Adiciona Dia, Mês e Ano do pagamento em todas linhas
    fun adicionar(diasArray: Array<String>,mesesArray: Array<String>, anosArray: Array<String>  ){
        for (i in 0..11){
            var counter = 6

            val anotv = TextView(this)
            val paramsAno = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsAno.setMargins(0, counter , 0, 0)
            anotv.layoutParams = paramsAno
            anotv.text = "${anosArray[i]}"
            anotv.textSize = 32.toFloat()
            anotv.setTypeface(Typeface.DEFAULT)
            anotv.setTextColor(resources.getColor(R.color.textColor2))
            anotv.gravity = CENTER
            tvYears.addView(anotv)

            val mestv = TextView(this)
            val paramsMes = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsMes.setMargins(0, counter , 0, 0)
            mestv.layoutParams = paramsMes
            mestv.text = "${mesesArray[i]}"
            mestv.textSize = 32.toFloat()
            mestv.setTypeface(Typeface.DEFAULT)
            mestv.setTextColor(resources.getColor(R.color.textColor2))
            mestv.gravity = START
            tvMonths.addView(mestv)

            val diatv = TextView(this)
            val paramsDia = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsDia.setMargins(0, counter , 0, 0)
            diatv.layoutParams = paramsDia
            diatv.text = "${diasArray[i]}"
            diatv.textSize = 32.toFloat()
            diatv.setTypeface(Typeface.DEFAULT)
            diatv.setTextColor(resources.getColor(R.color.textColor2))
            diatv.gravity = CENTER
            tvDays.addView(diatv)

        }
    }

    // Verifica o tipo de beneficiário e calcula as datas
    fun calcular(){
        val radioGroupChecked = intent.getStringExtra("radiobuttonChecked")
        val digito_cartao = intent.getIntExtra("digitoCartao", 0)
        when(digito_cartao){
            1 -> {

                if (radioGroupChecked == "Não"){

                    var dias_calculados = arrayOf("27","19","25","24","25","24","27","25","24","26","24","22")
                    var anoAux = arrayOf("2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
                else{
                    var dias_calculados = arrayOf("04","03","02","01","04","01","01","03","01","01","03","01")
                    var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
            }
            2 -> {
                if (radioGroupChecked == "Não"){
                    var dias_calculados = arrayOf("28","20","26","27","26","25","28","26","25","27","25","23")
                    var anoAux = arrayOf("2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")
                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
                else{
                    var dias_calculados = arrayOf("05","04","03","02","05","02","02","04","02","02","04","02")
                    var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")
                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
            }
            3 -> {
                if (radioGroupChecked == "Não"){
                    var dias_calculados = arrayOf("29","21","27","28","27","26","29","27","28","28","26","28")
                    var anoAux = arrayOf("2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
                else{
                    var dias_calculados = arrayOf("06","05","04","03","06","03","03","05","03","05","05","03")
                    var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
            }
            4 -> {
                if (radioGroupChecked == "Não"){
                    var dias_calculados = arrayOf("30","27","30","29","28","29","30","28","29","29","27","29")
                    var anoAux = arrayOf("2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
                else{
                    var dias_calculados = arrayOf("07","06","05","06","07","04","06","06","04","06","06","04")
                    var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
            }
            5 -> {
                if (radioGroupChecked == "Não"){
                    var dias_calculados = arrayOf("31","28","31","30","29","30","31","31","30","30","30","30")
                    var anoAux = arrayOf("2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
                else{
                    var dias_calculados = arrayOf("07","06","05","06","07","04","06","06","04","06","06","04")
                    var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                    adicionar(dias_calculados, mesesDoAno, anoAux)
                }
            }
            6 -> {
                    var dias_calculados = arrayOf("04","03","02","01","04","01","01","03","01","01","03","01")
                var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                adicionar(dias_calculados, mesesDoAno, anoAux)
            }
            7 -> {
                var dias_calculados = arrayOf("05","04","03","02","05","02","02","04","02","02","04","02")
                var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                adicionar(dias_calculados, mesesDoAno, anoAux)
            }
            8 -> {
                var dias_calculados = arrayOf("06","05","04","03","06","03","03","05","03","05","05","03")
                var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                adicionar(dias_calculados, mesesDoAno, anoAux)
            }
            9 -> {
                var dias_calculados = arrayOf("07","06","05","06","07","04","06","06","04","06","06","04")
                var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                adicionar(dias_calculados, mesesDoAno, anoAux)
            }
            0 -> {
                var dias_calculados = arrayOf("08","07","06","07","08","05","07","07","08","07","09","07")
                var anoAux = arrayOf("2021","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020","2020")

                adicionar(dias_calculados, mesesDoAno, anoAux)
            }
        }

    }
}


